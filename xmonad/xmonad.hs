import XMonad
import XMonad.Hooks.DynamicLog
import XMonad.Hooks.SetWMName
import XMonad.Hooks.ManageDocks
import XMonad.Util.NamedWindows
import XMonad.Util.Run
import XMonad.Util.Run(spawnPipe)
import XMonad.Util.EZConfig(additionalKeys)
import XMonad.Layout.Spacing
import XMonad.Prompt
import XMonad.Prompt.Shell
import System.IO
import XMonad.Actions.SimpleDate
import XMonad.Actions.WindowBringer
import XMonad.Util.SpawnOnce
import XMonad.Actions.FindEmptyWorkspace
import XMonad.Actions.GridSelect
import qualified XMonad.StackSet as W

myStartupHook :: X ()
myStartupHook = do
	spawnOnce "nitrogen --set-scaled --random pictures/wallpapers/ &"
	spawnOnce "xcompmgr &"
	spawnOnce "dunst &"
	spawnOnce "nm-applet &"
	spawnOnce "volumeicon &"
	spawnOnce "stalonetray --config .config/stalonetray/stalonetrayrc &"
	spawnOnce "transmission-daemon &"
	spawnOnce "setxkbmap -option ctrl:nocaps &"
	spawnOnce "xset -b &"
	spawnOnce "nepalical &"
	spawnOnce "export LC_CTYPE=en_US.UTF-8"
	spawnOnce "alias anki='anki --no-sandbox'"
	setWMName "LG3D"
	

main :: IO ()
main = do
    xmproc <- spawnPipe "xmobar /home/krrish/.config/xmobar/xmobarrc"

    -- xmonad $ def
    xmonad $ docks $ def
        { manageHook = manageDocks <+> manageHook def
        , layoutHook = avoidStruts $ spacingRaw True (Border 0 4 4 4) True (Border 4 4 4 4) True $layoutHook def
        , startupHook = myStartupHook
        , terminal = "st"
        , handleEventHook    = handleEventHook def
        , logHook = dynamicLogWithPP xmobarPP
                        { ppOutput = hPutStrLn xmproc
                        , ppTitle = xmobarColor "green" "" . shorten 50
                        }
        , modMask = mod4Mask
        , focusedBorderColor = "#6B8E23"
        } `additionalKeys`
        [ ((controlMask .|. mod1Mask, xK_l), spawn "slock")
        , ((controlMask, xK_Print), spawn "sleep 0.2; scrot -s -q 100 /home/krrish/pictures/%Y-%m-%d-%H:%M:%S.png")
        , ((mod1Mask, xK_m), spawn "st cmus")
        , ((mod4Mask .|. shiftMask, xK_q), spawn "poweroff")
        , ((mod4Mask, xK_p), spawn "dmenu_run -c -l 9 -p run: ")
        , ((mod4Mask, xK_d), date)
        , ((mod4Mask, xK_s), spawn "search")
        , ((mod4Mask, xK_y), spawn "st node /home/krrish/programs/youtube-cli-app/youtube.js")
        , ((mod4Mask .|. shiftMask, xK_r), spawn "reboot")
        , ((mod4Mask, xK_b), spawn "notify-send  'Firefox' 'Opening firefox'; firefox")
        , ((mod4Mask .|. controlMask, xK_x), shellPrompt def)
        , ((mod4Mask .|. shiftMask, xK_t), spawn "stalonetray -p")
        , ((mod4Mask .|. shiftMask, xK_n), spawn "st newsboat")
        , ((mod4Mask .|. shiftMask, xK_x), spawn "st vim .xmonad/xmonad.hs")
        , ((mod4Mask .|. shiftMask, xK_u), spawn "st sudo pacman -Syu")
        , ((mod4Mask .|. shiftMask, xK_e), spawn "st setxkbmap us")
        , ((mod4Mask .|. shiftMask, xK_v), spawn "st sudo protonvpn c -f")
        , ((mod4Mask .|. shiftMask, xK_i), spawn "notify-send 'Intellij Idea' 'Opening Intellij Idea'; /home/krrish/programs/idea/bin/idea.sh")
        , ((0, xK_Print), spawn "scrot -q 100 /home/krrish/pictures/%Y-%m-%d-%H:%M:%S.png")
        , ((0, 0x1008FF11), spawn "pactl set-sink-volume @DEFAULT_SINK@ -5%")
        , ((0, 0x1008FF13), spawn "pactl set-sink-volume @DEFAULT_SINK@ +5%")
        , ((mod4Mask .|. shiftMask, xK_m), spawn "pactl set-sink-volume @DEFAULT_SINK@ -5%")
        , ((mod4Mask .|. shiftMask, xK_p), spawn "pactl set-sink-volume @DEFAULT_SINK@ +5%")
        , ((0, 0x1008FF12), spawn "pactl set-sink-mute @DEFAULT_SINK@ toggle")
        , ((0, 0x1008FF02), spawn "sudo ~/scripts/brightness 50")
        , ((0, 0x1008FF03), spawn "sudo ~/scripts/brightness -50")
        , ((mod4Mask, xK_o), spawn "rofi -show run")

        , ((mod4Mask, xK_e), viewEmptyWorkspace)
        , ((mod4Mask .|. shiftMask, xK_e), tagToEmptyWorkspace)
        , ((mod4Mask .|. shiftMask, xK_f), spawnSelected def ["keepassxc", "brave", "pcmanfm"])
        , ((mod4Mask .|. shiftMask, xK_w), gotoMenu)
        ]    
